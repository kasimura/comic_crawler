package tracker

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3" // Register standard SQL driver for sqlite3

	"crawler/comic"
	"crawler/downloader"
)

const dbFileName = "comic.db"

func createSQLiteDB(d string) {
	l := d + "/" + dbFileName
	if _, err := os.Stat(l); os.IsNotExist(err) {
		file, err := os.Create(l)
		if err != nil {
			log.Fatal(err.Error())
		}
		file.Close()
	}
}

func createTableIfNotExists(db *sql.DB) {
	createComicTableSQL := `CREATE TABLE IF NOT EXISTS comic (
		"id" integer NOT NULL PRIMARY KEY,
		"name" TEXT,
		"url" TEXT
	  );`

	statement, err := db.Prepare(createComicTableSQL)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec()
	//log.Printf("Comic table created if it is not existed...\n")
}

func openSQLiteDB(d string) (func(), *sql.DB) {
	l := d + "/" + dbFileName
	db, _ := sql.Open("sqlite3", l)
	dbCloseFunc := func() {
		db.Close()
	}
	createTableIfNotExists(db)
	return dbCloseFunc, db
}

func insertComic(db *sql.DB, id, name, url string) {
	log.Println("Inserting comic record ...")
	insertComicSQL := `INSERT INTO comic(id, name, url) VALUES (?, ?, ?)`
	statement, err := db.Prepare(insertComicSQL)
	if err != nil {
		log.Fatalln(err.Error())
	}
	_, err = statement.Exec(id, name, url)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func fetchComics(db *sql.DB) map[int]string {
	selectComicSQL := `SELECT id, url FROM comic`
	row, err := db.Query(selectComicSQL)
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer row.Close()
	comicIDs := make(map[int]string)
	for row.Next() {
		var id int
		var url string
		row.Scan(&id, &url)
		comicIDs[id] = url
	}
	return comicIDs
}

func isComicInDB(db *sql.DB, id string) bool {
	selectComicSQL := `SELECT count(1) FROM comic WHERE id = ` + id
	row, err := db.Query(selectComicSQL)
	if err != nil {
		log.Fatalln(err.Error())
	}
	defer row.Close()
	result := 0
	for row.Next() {
		var count int
		row.Scan(&count)
		result += count
	}
	return result > 0
}

func trackBooksUpdate(url, d string) []int {
	c := comic.NewComic8(url)
	return downloader.TrackUpdate(d, c)
}

/*
GetComicsDownloaded ....
*/
func GetComicsDownloaded(d string) []string {
	dbCloseFunc, db := openSQLiteDB(d)
	defer dbCloseFunc()
	comicIDURLs := fetchComics(db)
	var comicURLs []string
	for _, v := range comicIDURLs {
		comicURLs = append(comicURLs, v)
	}
	return comicURLs
}

/*
StoreComic ...
*/
func StoreComic(c *comic.Comic8, d string) {
	dbCloseFunc, db := openSQLiteDB(d)
	defer dbCloseFunc()
	if !isComicInDB(db, c.Comic.ID) {
		insertComic(db, c.Comic.ID, c.Comic.Name, c.Comic.URL)
	}
}
