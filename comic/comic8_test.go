package comic

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

const comicID = "10403"

const comicURL = "https://www.comicbus.com/html/" + comicID + ".html"
const comicURLTemplate = "https://www.comicbus.com/html/%v.html"

func TestParseComicURL(t *testing.T) {
	u := parseURL(comicURL)
	want := "www.comicbus.com"
	got := u.Host
	if u.Host != want {
		t.Fatalf("Want %v, got %v", want, got)
	}
}

func TestGetComicURLs(t *testing.T) {
	comics := []string{
		`15790`,
		`10406`,
		`10403`,
		`105`,
		`1151`,
		`11753`,
		`13736`,
		`14132`,
		`14288`,
		`14625`,
		`14632`,
		`14898`,
		`15236`,
		`15704`,
		`15790`,
		`16312`,
		`7340`,
		`8751`,
	}
	for _, c := range comics {
		url := fmt.Sprintf(comicURLTemplate, c)
		comic := NewComic8(url)
		assert.NotEmpty(t, comic.Comic.ID)
		assert.NotEmpty(t, comic.Comic.Name)
		assert.NotEmpty(t, comic.Comic.URL)
		assert.NotEmpty(t, comic.Comic.Images)
	}
}

/*
func TestGetPageTokenByPageNumber(t *testing.T) {
	token := `g7b7eXS5G4BS3PPAe6G7v3ndaM7M6vK6Bc23vqDv`
	for i := 1; i < 131; i++ {
		getPageTokenByPageNumber(i, token)
	}
}
*/
