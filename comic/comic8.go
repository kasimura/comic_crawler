package comic

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/ditashi/jsbeautifier-go/jsbeautifier"
	"golang.org/x/net/html"
)

const htmlPath = "/html/"
const comicViewJS = "/js/comicview.js"

const emptyString = ""
const dot = "."
const lineCarriage = "\n"

const timeout = 5 * time.Second
const jsTag = "script"

const comicToken = `^var.*$`

const booksNum = `^for \(var i = 0; i <.*$`
const position = `^\s+var.*lc\(.*$`

const thumbPic = "https://www.comicabc.com/pics/0/%ss.jpg"
const all = "https://www.comicabc.com/comic/all.html"
const list = "https://www.comicabc.com/comic/list.html"
const update = "https://www.comicabc.com/comic/u-1.html"
const hot = "https://www.comicabc.com/comic/h-1.html"

/*
Version ...
*/
const Version = "0.1.1"

/*
Index ...
*/
type Index struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	Thumb   string `json:"thumb"`
	Version string `json:"version"`
}

/*
Comic ...
*/
type Comic struct {
	ID     string           `json:"id"`
	Name   string           `json:"name"`
	URL    string           `json:"url"`
	Images map[int][]string `json:"images"`
}

/*
Comic8 ...
*/
type Comic8 struct {
	Comic                Comic
	volumesTokens        string
	positions            []int
	totalVolumesNumber   int
	pagesNumberPosition  int
	serverNumberPosition int
	volumeNumberPosition int
}

/*
NewComic8 ...
*/
func NewComic8(u string) *Comic8 {
	c := &Comic{URL: u}
	c8 := &Comic8{Comic: *c}
	c8.CrawlImageURLs()
	return c8
}

/*
CrawlImageURLs ...
*/
func (c *Comic8) CrawlImageURLs() {
	log.Printf("Starting crawling comic from url %s\n", c.Comic.URL)
	c.parseComicValues()
	c.parseImagesURL()
}

func (c *Comic8) parseImagesURL() {
	c.Comic.Images = make(map[int][]string)
	for i := 0; i < c.totalVolumesNumber; i++ {
		tokenPerVolume := c.getComicTokenPerVolume(i)
		pagesToken := c.getPagesToken(tokenPerVolume)
		if i == 0 {
			c.getPositions(tokenPerVolume)
			c.getVolumeNumberPosition(tokenPerVolume)
			c.getRestNumbersByTokens(tokenPerVolume, pagesToken)
		}
		pagesNumberPosition := c.positions[c.pagesNumberPosition]
		pagesNumber := atoi(tokenPerVolume[pagesNumberPosition:(pagesNumberPosition + 2)])
		var imageURLs []string
		for j := 1; j <= pagesNumber; j++ {
			imageURLs = append(imageURLs, c.getURLFromTemplate(j, tokenPerVolume, getPageSuffix(j, pagesToken)))
		}
		c.Comic.Images[atoi(tokenPerVolume[c.positions[c.volumeNumberPosition]:c.positions[c.volumeNumberPosition]+2])] = imageURLs
	}
}

func (c *Comic8) parseComicValues() {
	u := parseURL(c.Comic.URL)
	c.getComicID(u)
	c.getComicName(u.String())
	allPossiableURLs := getComicEnteranceURLs(u, c.Comic.ID)
	targetURL, e := pingURLs(allPossiableURLs, timeout)
	targetURL = strings.Replace(targetURL, "c-", "a-", 1) // Even c-* url still could ping, but seems 8comic is obsoleting...
	log.Printf("Comic enterance URL: %s\n", targetURL)
	if e != nil {
		log.Fatalf("All URLs found are not reachable!")
	}
	js := getHTMLTagElement(targetURL, jsTag)
	c.volumesTokens = getComicTokens(js)
	c.totalVolumesNumber = getComicTotalVolumes(js)
	c.positions = getComicPositions(js)

}

func (c *Comic8) getComicName(url string) {
	result := getHTMLTagElement(url, "title")
	keys := make(map[string]bool)
	list := []string{}

	for _, entry := range result {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	c.Comic.Name = list[0][:strings.Index(list[0], " ")]
}

func getHTMLContent(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatal("ERROR: Failed to crawl JS \"" + url + "\"")
		return "", err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	return string(body), err
}

func getHTMLTagElement(url string, e string) []string {
	var res []string
	resp, err := http.Get(url)

	if err != nil {
		log.Fatal("ERROR: Failed to crawl \"" + url + "\"")
		return res
	}

	b := resp.Body
	defer b.Close() // close Body when the function returns
	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		bodyString := string(bodyBytes)
		z := html.NewTokenizer(strings.NewReader(bodyString))
		pt := z.Token()
	loopDoc:
		for {
			tt := z.Next()
			switch tt {
			case html.ErrorToken:
				break loopDoc
			case html.StartTagToken, html.SelfClosingTagToken, html.EndTagToken:
				pt = z.Token()
			case html.TextToken, html.DoctypeToken:
				if pt.Data != e {
					continue
				}
				res = append(res, strings.TrimSpace(html.UnescapeString(string(z.Text()))))
			}
		}
	}
	return res
}

func findJSTarget(js []string, tgt string) string {
	sort.Strings(js)
	s := strings.Join(js, emptyString)
	options := jsbeautifier.DefaultOptions()
	s, err := jsbeautifier.Beautify(&s, options)
	if err != nil {
		log.Fatal(err)
	}
	js = strings.Split(s, lineCarriage)

	validToken := regexp.MustCompile(tgt)

	max := 0
	idx := -1
	for i, j := range js {
		if len(j) > max && validToken.MatchString(j) {
			max = len(j)
			idx = i
		}
	}
	if idx == -1 {
		return ""
	}
	return js[idx]
}

func (c *Comic8) getComicID(url url.URL) {
	u := strings.ReplaceAll(url.EscapedPath(), htmlPath, emptyString)
	c.Comic.ID = u[:strings.Index(u, dot)]
}

func getComicEnteranceURLs(url url.URL, comicID string) []string {
	url.Path = comicViewJS
	s, err := getHTMLContent(url.String())
	if err != nil {
		log.Fatal(err)
	}
	re := regexp.MustCompile(`baseurl=".*"`)
	urls := re.FindAllString(s, -1)
	for i := range urls {
		urls[i] = strings.ReplaceAll(urls[i], `baseurl=`, emptyString)
		urls[i] = strings.ReplaceAll(urls[i], `"`, emptyString)
		urls[i] = urls[i] + comicID + ".html"
	}
	return urls
}

func parseURL(comicURL string) url.URL {
	u, err := url.Parse(comicURL)
	if err != nil {
		log.Fatal(err)
	}
	return *u
}

func isTitleElement(n *html.Node) bool {
	return n.Type == html.ElementNode && n.Data == "title"
}

func traverse(n *html.Node) (string, bool) {
	if isTitleElement(n) {
		return n.FirstChild.Data, true
	}

	for c := n.FirstChild; c != nil; c = c.NextSibling {
		result, ok := traverse(c)
		if ok {
			return result, ok
		}
	}

	return "", false
}

func getHTMLTitle(r io.Reader) (string, bool) {
	doc, err := html.Parse(r)
	if err != nil {
		panic("Fail to parse html")
	}

	return traverse(doc)
}

func isClosed(ch <-chan string) bool {
	select {
	case <-ch:
		return true
	default:
	}

	return false
}

func pingURLs(urls []string, timeout time.Duration) (string, error) {
	ch := make(chan string)
	defer close(ch)
	for _, url := range urls {
		go func(url string) {
			resp, err := http.Get(url)
			if err == nil {
				defer resp.Body.Close()
				if title, ok := getHTMLTitle(resp.Body); ok {
					if title != `404.找不到頁面` {
						if !isClosed(ch) {
							ch <- url
						}
					}
				}
			}
		}(url)
	}

	select {
	case res := <-ch:
		return res, nil
	case <-time.After(timeout):
		return emptyString, fmt.Errorf("timed out waiting for return")
	}
}

func getComicTokens(js []string) string {
	fullToken := findJSTarget(js, comicToken)
	t := fullToken[strings.Index(fullToken, "'")+1 : strings.LastIndex(fullToken, "'")]
	surplus := `546865496d67696d67636f6d69632e6a7067`
	if len(t) <= len(surplus) {
		return ""
	}
	if strings.LastIndex(t, surplus) == -1 {
		return t
	}
	return t[0:strings.LastIndex(t, surplus)]
}

func getComicTotalVolumes(js []string) int {
	lessThan := "< "
	vol := findJSTarget(js, booksNum)
	volume := vol[strings.Index(vol, lessThan)+len(lessThan) : strings.LastIndex(vol, ";")]
	res, err := strconv.Atoi(volume)
	if err != nil {
		return -1
	}
	return res
}

func atoi(s string) int {
	if len(s) != 2 {
		return -1
	}
	az := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	a := s[0:1]
	b := s[1:2]
	if a == "Z" {
		return (8000 + strings.Index(az, b))
	}
	return (strings.Index(az, a)*52 + strings.Index(az, b))
}

func getComicPositions(js []string) []int {
	sort.Strings(js)
	s := strings.Join(js, "")
	options := jsbeautifier.DefaultOptions()
	s, err := jsbeautifier.Beautify(&s, options)
	if err != nil {
		log.Fatal(err)
	}
	js = strings.Split(s, "\n")

	validToken := regexp.MustCompile(position)

	var poss []int
	for _, j := range js {
		if validToken.MatchString(j) {
			start := strings.LastIndex(j, " + ") + len(" + ")
			end := strings.LastIndex(j, ", 2")
			if start >= 0 && end > 0 {
				p, err := strconv.Atoi(j[strings.LastIndex(j, " + ")+len(" + ") : strings.LastIndex(j, ", 2")])
				if err == nil {
					poss = append(poss, p)
				}
			}
		}
	}
	return poss
}

func (c *Comic8) getComicTokenPerVolume(i int) string {
	res := len(c.volumesTokens) / c.totalVolumesNumber
	return c.volumesTokens[i*res : (i+1)*res]
}

func (c *Comic8) getPagesToken(tokenPerVolume string) string {
	pos := 0
	for _, v := range c.positions {
		if v <= 6 {
			pos = v + 2
		} else {
			break
		}
	}
	return tokenPerVolume[pos:(40 + pos)]
}

func (c *Comic8) getPositions(tokenPerVolume string) {
	for i, v := range c.positions {
		j := atoi(tokenPerVolume[v:(v + 2)])
		if j < 0 {
			c.positions = remove(c.positions, i)
		}
	}
}

func remove(slice []int, s int) []int {
	return append(slice[:s], slice[s+1:]...)
}

func (c *Comic8) getVolumeNumberPosition(tokenPerVolume string) {
	i := 0
	r := atoi(tokenPerVolume[c.positions[i]:(c.positions[i] + 2)])
	for j, p := range c.positions {
		m := atoi(tokenPerVolume[p:(p + 2)])
		if m < r {
			r = m
			i = j
		}
	}
	c.volumeNumberPosition = i
}

func (c *Comic8) getRestNumbersByTokens(tokenPerVolume, pagesToken string) {
	possibleFirstImageURLsPositions := c.getFirstImageURLs(tokenPerVolume, pagesToken)
	possibleFirstImageURLs := getPossibleImageURLsSlice(possibleFirstImageURLsPositions)
	validImageURL := pingImageURLs(possibleFirstImageURLs)
	c.serverNumberPosition = possibleFirstImageURLsPositions[validImageURL]
	c.getPageNumberPosition()
}

func (c *Comic8) getFirstImageURLs(tokenPerVolume, pagesToken string) map[string]int {
	var result = make(map[string]int)
	template := `http://img%v.8comic.com/%v/` + c.Comic.ID + `/` + strconv.Itoa(atoi(tokenPerVolume[c.positions[c.volumeNumberPosition]:c.positions[c.volumeNumberPosition]+2])) + `/001_` + pagesToken[0:3] + `.jpg`
	for i, n := range c.positions {
		if i == c.volumeNumberPosition {
			continue
		}
		s := strconv.Itoa(atoi(tokenPerVolume[n:(n + 2)]))
		if len(s) != 2 {
			continue
		}
		url := fmt.Sprintf(template, s[0:1], s[1:2])
		result[url] = i
	}
	return result
}

func getPossibleImageURLsSlice(mapURLs map[string]int) []string {
	urls := make([]string, 0, len(mapURLs))
	for u := range mapURLs {
		urls = append(urls, u)
	}
	return urls
}

func pingImageURLs(urls []string) string {
	if len(urls) <= 0 {
		return ""
	}
	if len(urls) == 1 {
		return urls[0]
	}
	result, err := pingImages(urls, timeout)
	if err != nil {
		return ""
	}
	return result
}

func pingImages(urls []string, timeout time.Duration) (string, error) {
	ch := make(chan string)
	defer close(ch)
	for _, url := range urls {
		go func(url string) {
			resp, err := http.Get(url)
			if err == nil {
				if resp.StatusCode == 200 {
					ch <- url
				}
			}
		}(url)
	}

	select {
	case u := <-ch:
		return u, nil
	case <-time.After(timeout):
		return emptyString, fmt.Errorf("timed out waiting for return")
	}
}

func (c *Comic8) getPageNumberPosition() {
	c.pagesNumberPosition = len(c.positions) - c.volumeNumberPosition - c.serverNumberPosition
}

func getPageSuffix(i int, pagesToken string) string {
	winLength := 3
	start := ((i - 1) / 10 % 10) + (((i - 1) % 10) * 3)
	end := start + winLength
	return pagesToken[start:end]
}

func (c *Comic8) getURLFromTemplate(p int, t1, t2 string) string {
	template := `https://img%v.8comic.com/%v/%v/%d/%v_%v.jpg`
	serverNumber := strconv.Itoa(atoi(t1[c.positions[c.serverNumberPosition] : c.positions[c.serverNumberPosition]+2]))
	volumeNumber := atoi(t1[c.positions[c.volumeNumberPosition] : c.positions[c.volumeNumberPosition]+2])
	return fmt.Sprintf(template, serverNumber[0:1], serverNumber[1:2], c.Comic.ID, volumeNumber, fmt.Sprintf("%03v", p), t2)
}

/*
FindIndices ...
*/
func FindIndices(qs string, choice string) []Index {
	url := all
	var result []Index
	switch choice {
	case "h":
		url = hot
	case "u":
		url = update
	case "a":
	default:
	}
	result = findIndces(surfIndicesURL(url), qs)
	if choice == "a" {
		result = append(result, findIndces(surfIndicesURL(list), qs)...)
	}
	sort.Sort(indexList(result))
	return result
}

func surfIndicesURL(url string) *goquery.Document {
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	return doc
}

func indexExists(indices []Index, id string) bool {
	for _, index := range indices {
		if index.ID == id {
			return true
		}
	}

	return false
}

func findIndces(doc *goquery.Document, qs string) []Index {
	var indices []Index

	doc.Find("div div div").Each(func(i int, s *goquery.Selection) {
		node := s.Find("a")
		link, _ := node.Attr("href")
		if strings.Contains(link, "/html/") {
			name := node.Text()
			ren := regexp.MustCompile(`\s+`)
			name = ren.ReplaceAllString(name, ``)
			ren = regexp.MustCompile(`漫畫\d+`)
			name = ren.ReplaceAllString(name, ``)
			ren = regexp.MustCompile(`更新.*$`)
			name = ren.ReplaceAllString(name, ``)
			if len(qs) == 0 || strings.Contains(name, qs) {
				re := regexp.MustCompile(`\d+`)
				id := string(re.Find([]byte(link)))
				if !indexExists(indices, id) {
					t := fmt.Sprintf(thumbPic, id)
					c := Index{
						ID:      id,
						Name:    name,
						Thumb:   t,
						Version: Version,
					}
					indices = append(indices, c)
				}
			}
		}
	})
	return indices
}

type indexList []Index

func (e indexList) Len() int {
	return len(e)
}

func (e indexList) Less(i, j int) bool {
	return e[i].ID > e[j].ID
}

func (e indexList) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

/*
ConvertImagesToList ...
*/
func (c *Comic8) ConvertImagesToList() Comic {
	var keys []int
	for k := range c.Comic.Images {
		keys = append(keys, k)
	}

	sort.Ints(keys)

	//for _, k := range keys {
	//fmt.Printf("k : %v\n", k)
	//c.Comic.Images = append(c.Comic.Images, c.Comic.Images[k]...)
	//}
	json := c.Comic
	return json
}
