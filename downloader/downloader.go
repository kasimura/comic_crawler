package downloader

import (
	"archive/tar"
	"compress/gzip"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"

	"crawler/comic"
)

const maxConn = 500
const minConn = 100

type item struct {
	src, tgt string
}

func downloadImage(image, d string, ch chan string, chFinished chan bool) {
	defer func() {
		chFinished <- true
	}()
	tokens := strings.Split(image, "/")
	filePath := d + "/" + tokens[len(tokens)-3] + "/" + tokens[len(tokens)-2]
	fileName := tokens[len(tokens)-2] + "_" + tokens[len(tokens)-1]

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		os.MkdirAll(filePath, 0700)
	}

	if _, err := os.Stat(filePath + "/" + fileName); os.IsNotExist(err) {
		output, err := os.Create(filePath + "/" + fileName)
		if err != nil {
			log.Fatal("Error while creating", fileName, "-", err)
		}
		defer output.Close()

		res, err := http.Get(image)
		if err != nil {
			log.Fatal("http get error: ", err)
		} else {
			defer res.Body.Close()
			_, err = io.Copy(output, res.Body)
			if err != nil {
				log.Fatal("Error while downloading", image, "-", err)
			}
		}

		if err != nil {
			log.Fatal("ERROR: Failed to crawl \"" + image + "\"")
			return
		}
	}
}

func lpad(s string, pad string, plength int) string {
	for i := len(s); i < plength; i++ {
		s = pad + s
	}
	return s
}

func findPath(id, d string) string {
	return d + "/" + id
}

func findItems(path string) []item {
	dirs, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	items := make([]item, len(dirs))

	ic := 0
	for i, d := range dirs {
		if d.IsDir() {
			tgt := path + "/" + lpad(d.Name(), "0", 4) + ".tar.gz"
			src := path + "/" + d.Name() + "/"
			var it item
			it.src = src
			it.tgt = tgt
			items[i] = it
			ic++
		}
	}
	return items
}

func findDownloaded(path string) []int {
	files, err := ioutil.ReadDir(path + "/")
	var its []int
	if err != nil {
		return its
	}

	for _, f := range files {
		if !f.IsDir() {
			i := strings.IndexRune(f.Name(), '.')
			m := f.Name()[0:i]
			n, err := strconv.ParseInt(m, 10, 64)
			if err == nil {
				m = strconv.FormatInt(n, 10)
			}
			r, _ := strconv.Atoi(m)
			its = append(its, r)
		}
	}
	sort.Ints(its)
	return its
}

func downloadImages(d string, images []string, batchSize int) {
	chImages := make(chan string, batchSize)
	chFinished := make(chan bool)
	for i, j := 0, batchSize; i < len(images); i, j = i+batchSize, j+batchSize {
		if j >= len(images) {
			j = len(images)
		}
		for _, u := range images[i:j] {
			go downloadImage(u, d, chImages, chFinished)
		}
	wait:
		for c := 0; c < batchSize; {
			select {
			case image := <-chImages:
				log.Printf("Crawl image from url %v\n", image)
			case <-chFinished:
				c++
				if c == j-i {
					break wait
				}
			}
		}
	}
	close(chImages)
	close(chFinished)
}

/*func defaultDirectory() string {
	loc := "comicbus"
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr.HomeDir + "/" + loc
}*/

/*func getDefaultDirectory(loc string) string {
	if _, err := os.Stat(loc); os.IsNotExist(err) {
		os.Mkdir(loc, 0700)
	}
	return loc
}*/

func uniqueItems(items []item) []item {
	mapItems := make(map[item]int)
	lc := 0
	for _, entry := range items {
		if entry.src != "" && entry.tgt != "" {
			mapItems[entry]++
			lc++
		}
	}
	nItems := make([]item, lc)
	ic := 0
	for item := range mapItems {
		nItems[ic] = item
		ic++
	}
	return nItems
}

func tarAllDirectories(id, def string) []item {
	items := uniqueItems(findItems(findPath(id, def)))
	ch := make(chan bool, len(items))
	for _, it := range items {
		log.Printf("Archiving to %s\n", it.tgt)
		go func(src, tgt string) {
			ch <- compress(src, tgt)
		}(it.src, it.tgt)
	}

	for range items {
		<-ch
	}
	return items
}

func removeAllTaredDirectories(items []item) {
	chErr := make(chan error, len(items))
	for _, it := range items {
		if it.src != "" && it.tgt != "" {
			go func(src string) {
				chErr <- os.RemoveAll(src)
			}(it.src)
		} else {
			chErr <- nil
		}
	}

	for range items {
		<-chErr
	}
}

/*
func runSed(s string, cmd string) (output string) {
	engine, err := sed.New(strings.NewReader(cmd))
	if err != nil {
		log.Fatal(err)
	}
	output, err = engine.RunString(s)
	if err != nil {
		log.Fatal(err)
	}
	return
}
*/

/*
TrackUpdate ...
*/
func TrackUpdate(d string, c *comic.Comic8) []int {
	files := findDownloaded(findPath(c.Comic.ID, d))
	var its []int
	for k := range c.Comic.Images {
		i := sort.Search(len(files), func(i int) bool { return k <= files[i] })
		if i < len(files) && files[i] == k {
			log.Printf("Comic ID: %v name : %v of volume [%v] has been downloaded", c.Comic.ID, c.Comic.Name, k)
		} else {
			log.Printf("There is an update for comic ID: %v name : %v of volume [%v]", c.Comic.ID, c.Comic.Name, k)
			its = append(its, k)
		}
	}
	sort.Ints(its)
	return its
}

/*
Download ...
*/
func Download(d string, c *comic.Comic8, connNum int) {
	if connNum > maxConn {
		log.Printf("Reset go routines number to %d (maximum limit) as user set to %d which exceeded maximum limit", maxConn, connNum)
		connNum = maxConn
	}

	if connNum < minConn {
		log.Printf("Reset go routines number to %d (minium limit) as user set to %d which exceeded minium limit", minConn, connNum)
		connNum = minConn
	}

	if len(c.Comic.Images) > 0 {
		books := TrackUpdate(d, c)
		for _, book := range books {
			log.Printf("Comic ID: %v name : %v is downloading volume [%v]...\n", c.Comic.ID, c.Comic.Name, book)
			downloadImages(d, c.Comic.Images[book], connNum)
		}
	}

	removeAllTaredDirectories(tarAllDirectories(c.Comic.ID, d))
}

func walk(path, r string, tw *tar.Writer) error {
	if tw == nil {
		panic("tw must not be nil")
	}

	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	i, err := f.Stat()
	if err != nil {
		return err
	}

	h, err := tar.FileInfoHeader(i, "")
	if err != nil {
		return err
	}
	h.Name = strings.TrimPrefix(path, r)

	if err = tw.WriteHeader(h); err != nil {
		return err
	}

	if i.IsDir() {
		fs, err := ioutil.ReadDir(path)
		if err != nil {
			return err
		}

		for i := range fs {
			p := path + fs[i].Name()
			//p := path + "/" + fs[i].Name()
			if err = walk(p, r, tw); err != nil {
				return err
			}
		}

		return nil
	}

	_, err = io.Copy(tw, f)
	return err
}

func compress(src, tgt string) bool {
	log.SetFlags(log.Lshortfile)

	dst, err := os.Create(tgt)
	if err != nil {
		log.Fatalln(err)
	}
	defer dst.Close()

	gz := gzip.NewWriter(dst)
	tw := tar.NewWriter(gz)

	info, err := os.Stat(src)
	if err != nil {
		log.Println(err)
		return false
	}

	r := strings.TrimSuffix(src, "/")
	r = strings.TrimSuffix(r, info.Name())
	err = walk(src, r, tw)
	if err != nil {
		log.Print(err)
		return false
	}

	if err = tw.Close(); err != nil {
		log.Print(err)
		return false
	}

	if err = gz.Close(); err != nil {
		log.Print(err)
		return false
	}
	return true
}
