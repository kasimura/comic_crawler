package downloader

import (
	"fmt"
	"testing"
)

const comicID = `15236`

//const url = `https://comicbus.com/html/` + comicID + `.html`

func TestFindPath(t *testing.T) {
	d := `/home/kasim/comicbus`
	want := `/home/kasim/comicbus/` + comicID
	got := findPath(comicID, d)
	if got != want {
		t.Fatalf("Want %v but got %v", want, got)
	}
}

func TestTarAllDirectories(t *testing.T) {
	def := `/home/kasim/comicbus`
	//removeAllTaredDirectories(tarAllDirectories(url, def))
	p := findPath(comicID, def)
	fmt.Println(p)
	files := findDownloaded(p)
	fmt.Println(files)
}
