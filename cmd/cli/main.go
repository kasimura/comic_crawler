package main

import (
	"flag"
	"log"
	"net/url"
	"os"
	"os/user"

	"github.com/urfave/cli"

	"crawler/comic"
	"crawler/downloader"
	"crawler/tracker"
)

var c = flag.String("c", "comicbus", "The default directory to store your downloaded comic")
var k = flag.Int("k", 500, "The go routine number to crawl comic")

func defaultDirectory() string {
	loc := "comicbus"
	usr, err := user.Current()
	if err != nil {
		log.Fatal(err)
	}
	return usr.HomeDir + "/" + loc
}

func getDefaultDirectory(loc string) string {
	if _, err := os.Stat(loc); os.IsNotExist(err) {
		os.Mkdir(loc, 0700)
	}
	return loc
}

func main() {
	app := cli.NewApp()
	app.Name = "8comic crawler"
	app.Usage = "a crawler to download comic on 8comic.com with gorountines"
	app.Version = comic.Version

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "url, u",
			Value: "",
			Usage: "URL of comic downloading",
		},
		cli.StringFlag{
			Name:  "location, l",
			Value: defaultDirectory(),
			Usage: "Location for comic downloading",
		},
		cli.UintFlag{
			Name:  "connection, k",
			Value: 100,
			Usage: "The number of connection of gorountines to download images",
		},
	}

	cli.VersionFlag = cli.BoolFlag{
		Name:  "version, v",
		Usage: "print verson",
	}

	app.Action = func(c *cli.Context) error {
		u := c.String("url")
		if c.NArg() > 0 {
			arg0 := c.Args().Get(0)
			v, err := url.Parse(arg0)
			if err == nil && u == "" {
				u = v.String()
			}
		}
		d := getDefaultDirectory(c.String("location"))
		connNum := c.Int("connection")
		var comicURLs []string
		if u == "" {
			comicURLs = tracker.GetComicsDownloaded(d)
			if len(comicURLs) <= 0 {
				log.Fatalf("As no record in the database, please provide 8comic URL for downloading comic! e.g. cralwer -u https://www.comicbus.com/html/17198.html")
			}
		} else {
			comicURLs = []string{u}
		}

		for _, url := range comicURLs {
			c := comic.NewComic8(url)
			tracker.StoreComic(c, d)
			if len(c.Comic.Images) > 0 {
				downloader.Download(d, c, connNum)
			}
		}
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
