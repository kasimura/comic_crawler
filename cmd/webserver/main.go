package main

import (
	"os"

	"github.com/gin-gonic/gin"

	"crawler/comic"
)

const prefix = "https://www.comicabc.com/comic/"
const suffix = ".html"
const all = "a"
const update = "u"
const hot = "h"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		//log.Fatal("$PORT must be set")
		port = "8000"
	}

	r := gin.Default()
	r.GET("/version", func(c *gin.Context) {
		c.JSON(200, comic.Version)
	})
	r.GET("/comic/:id", func(c *gin.Context) {
		id := c.Param("id")
		url := prefix + id + suffix
		comic := comic.NewComic8(url)
		resp := comic.ConvertImagesToList()
		c.JSON(200, resp)
	})
	r.GET("/", func(c *gin.Context) {
		comics := comic.FindIndices("", all)
		c.JSON(200, comics)
	})
	r.GET("/search/:keyword", func(c *gin.Context) {
		keyword := c.Param("keyword")
		if len(keyword) <= 0 || len(keyword) > 40 {
			c.JSON(404, gin.H{
				"err_code": 9,
				"message":  "Keyword to search is empty or too long (10)",
			})
			return
		}
		comics := comic.FindIndices(keyword, all)
		c.JSON(200, comics)
	})
	r.GET("/update", func(c *gin.Context) {
		comics := comic.FindIndices("", update)
		c.JSON(200, comics)
	})
	r.GET("/hot", func(c *gin.Context) {
		comics := comic.FindIndices("", hot)
		c.JSON(200, comics)
	})
	r.Run(":" + port)
}
